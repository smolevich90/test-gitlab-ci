<?php

if (ini_get('opcache.enable') != 1) {
    ini_set('opcache.enable', 1);
}

$sFileName = './target.php';

echo( $sFileName . ' EXISTS? : ' . ( file_exists($sFileName) ? 'Y' : "N" ) );

require_once( $sFileName );

echo( "<BR/>FILE INCLUDED!" );
